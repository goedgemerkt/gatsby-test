import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Image from '../components/image';
import '../css/miniNaamstickers.css'

const ProductPage = () => (
  <Layout style={{ maxWidth: '1140px', margin: '0 auto' }}>
    <SEO title="Home" />

    <div style={{ display: 'flex', marginTop: '30px' }}>

      <div class="product_info" style={{ width: '50%', marginRight: '15px' }}>
        <Image
          style={{ borderRadius: '8px' }}
          src="product_img.jpg"
          alt="Mini naamstickers"
        />
        <div class="product product-tab description item content">
          <div class="block-title"><strong>Omschrijving</strong></div>
          <div class="block-content"><p>Zorg ervoor dat je geen kleine voorwerpen meer verliest en merk ze met deze Mini Naamstickers.</p>
            <h2 style={{ fontFamily: "'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif", fontSize: "14px" }}>Transparante naamstickers</h2>
            <p>Transparante Mini Naamstickers zijn een ideale oplossing voor het markeren van kleine producten of als je het belangrijk vindt dat de stickers minder opvallen op het voorwerp. Als je de transparante stickers op donkere voorwerpen plakt is het handig om te kiezen voor een fellere kleur tekst.&nbsp;</p>
            <h2 style={{ fontFamily: "'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif", fontSize: "14px" }}>Instructies Mini Naamstickers</h2>
            <p>Omdat het oppervlak van de minilabels kleiner is kan het voorkomen dat het label minder goed blijft zitten.&nbsp;De hechting wordt sterker, als&nbsp;je het label&nbsp;24 uur "rusten".</p>
            <p>Onze labels gaan minimaal 200 wasbeurten mee en verkleuren niet. (Bij extreme hitte, zoals bij gebruik van een stoom-/magnetron-sterilisator kan de structuur van de labels veranderen en de kleur vervagen).</p>
            <p>Liever een ander formaat? Bekijk dan onze kleine en grote naamstickers.</p></div>
        </div>

        <div class="data item content" id="product_info_shipping" data-role="content">
          <div class="product product-tab shipping_information">
            <div class="block-title"><strong>Verzenden</strong></div>
            <div class="block-content">Alle Goedgemerkt producten worden verzonden met de reguliere post. Je hoeft dus niet thuis te blijven, jouw nieuwe producten worden namelijk gewoon door de brievenbus gedaan.</div>
          </div>
        </div>

        <div class="data item content" id="product_info_usage_information" data-role="content">
          <div class="product product-tab usage_information">
            <div class="block-title"><strong>Tips voor gebruik</strong></div>
            <div class="block-content"><p>Plak de Naamsticker op een schoon, vlak en droog oppervlak en laat de sticker minimaal 8 uur hechten voordat je deze in de vaatwasser doet. Zorg ervoor dat gemerkte producten niet tegen elkaar aan staan in de vaatwasser. De stickers zijn ook magnetron en diepvries bestendig.</p>
              <p>Onze labels gaan minimaal 200 wasbeurten mee en verkleuren niet. Let wel op bij extreme hitte. Bij bijvoorbeeld het heetste programma van de vaatwasser of bij steriliseren kan de structuur van de labels veranderen en de kleur vervagen.</p></div>
          </div>
        </div>

      </div>

      <div style={{ width: '50%', marginLeft: '15px', fontSize: '16px' }}>
        <div class="cta_wrapper">
          <h1 style={{marginBottom: '5px'}}>Mini Naamstickers</h1>
          <p>Op deze Mini Naamstickers kun je je eigen tekst zetten, bijvoorbeeld de voor- en achternaam van je kindje of een telefoonnummer.<br />
Ideaal om kleine spullen te merken, zoals potloden, liniaaltjes, passer, tandenborstels, USB-stick, horloge en (zonne)bril maar ook babyspulletjes, kortom overal waar je mini naamstickers voor nodig hebt.</p>
          <ul class="checkmarks">
            <li>De Mini Naamstickers zijn beschikbaar met gekleurde achtergrond en witte tekst, transparant met gekleurde tekst en wit met gekleurde tekst. </li>
            <li>Vaatwasser- &amp; magnetronveilig.</li>
            <li>Formaat: 3,8 x 0,6 cm.</li>
          </ul>
          <span class="price">€&nbsp;10,49</span>
          <span class="labels">40 labels</span>
          <button class="design-label-btn" type="button" title="Ontwerp jouw labels">Ontwerp jouw labels</button>
        </div>
      </div>

    </div>

  </Layout>
)

export default ProductPage
