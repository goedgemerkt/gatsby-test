import React from "react"

import Layout from "../components/layout"
import Image from "../components/image"
import Quicklinks from '../components/quicklinks.js'
import Usps from '../components/usps'
import SEO from "../components/seo"
import '../css/index.css'

const IndexPage = () => (
  <Layout style={{ maxWidth: '1140px', margin: '0 auto' }}>
    <SEO title="Home" />
    <div className="hero" style={{ width: '100%', height: '100%' }}>
      <Image
        src="banner_januari_desktop_nl.jpg"
        className="mx-auto shadow-xl"
        alt="home"
      />
    </div>

    <Quicklinks />
    <Usps />
  </Layout>
)

export default IndexPage
