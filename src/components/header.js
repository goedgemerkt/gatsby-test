import { Link } from "gatsby"
import PropTypes from "prop-types"
import styled from '@emotion/styled';
import React from "react"
import BackgroundImage from 'gatsby-background-image';
import SingleImage from './singleImage';
import Image from './image';

const ImageBackground = styled(BackgroundImage)`
  background-position: left center;
  background-size: contain;
  height: 125px;
  display: flex;

  /* override the default margin for sibling elements  */
  + * {
    margin-top: 0;
  }
`;

const Header = () => {

  const image = SingleImage({
    src: 'ggm-header.jpg'
  });

  return (
      <Link
        to="/"
        style={{
          color: `white`,
          textDecoration: `none`,
        }}
      >
        <ImageBackground style={{
          marginBottom: `1.45rem`,
        }} Tag="div" fluid={image.fluid} fadeIn="soft">
          <div style={{display: 'flex', color: '#005380', fontFamily: 'arial', width: 'fit-content', margin: 'auto 0 5px auto'}}>
        <div style={{display: 'flex', marginRight: '30px'}}>
        <Image
          src="Delivery.svg"
          className="mx-auto shadow-xl"
          alt="Delivery"
          style={{margin: 'auto 0', height: '70px'}}
          height="70"
          width="70"
        />
        <p style={{margin: 'auto 0', fontSize: '13px', marginLeft: '5px'}}>Verzending binnen 24 uur!</p>
        </div>
        <div style={{display: 'flex'}}>
        <Image
          src="Helpdesk.svg"
          className="mx-auto shadow-xl"
          alt="helpdesk"
          style={{margin: 'auto 0', height: '50px'}}
          height="50"
          width="50"
        />
        <p style={{margin: 'auto 0', fontSize: '13px', marginLeft: '5px'}}>Vragen? +31 (0)33-461-5834</p>
        </div>
      </div>
        </ImageBackground>
      </Link>)
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
