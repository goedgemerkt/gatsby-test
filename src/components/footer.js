import React from "react"
import '../css/footer.css'

const Footer = () => {

  return (
        <footer>
            <div>
                <h2>Assortiment</h2>
                <ul>
                    <li>Naamstickers</li>
                    <li>Kledingstempel</li>
                    <li>Kleine Naamstickers</li>
                    <li>Grote Naamstickers</li>
                    <li>Plakgemak</li>
                    <li>SOS Naambandje</li>
                    <li>Tassenhanger</li>
                </ul>
            </div>
            <div>
                <h2>Producten</h2>
                <ul>
                    <li>Kleine naamstickers</li>
                </ul>
            </div>
            <div>
                <h2>Goedgemerkt</h2>
                <ul>
                    <li>Onze goedgemerkjes</li>
                </ul>
            </div>
            <div>
                <h2>Handige informatie</h2>
                <ul>
                    <li>De beste labeltips&trucs</li>
                </ul>
            </div>
        </footer>
      )
}

export default Footer