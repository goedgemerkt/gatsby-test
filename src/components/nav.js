import React from "react"
import Image from './image';
import { Link, StaticQuery, graphql } from "gatsby"

const Nav = () => {
  return (
      <div className="navigation-wrapper">
        <StaticQuery query={graphql`
        query MyQuery {
          goedgemerkt {
            categoryList {
              children {
                id
                name
                url_path
              }
            }
          }
        }
        `} render={data => (
          <>
          <ul>
            <li>
            <Link to="/">
              <Image
                src="Home-w.svg"
                className="mx-auto shadow-xl"
                alt="home"
                width="32"
                height="32"
                />
                </Link>
                </li>
            {data.goedgemerkt.categoryList[0].children.map((item, i) => (
              i < 6 ? <li key={i}><Link style={{color: '#fff', textDecoration: 'none'}} to={`/${item.url_path}`}>{item.name}</Link></li> : <></>
            ))}
          </ul>
          <div key={'akskaska'}>
          <Link to="/">
            <Image
              src="Cart-white.svg"
              className="mx-auto shadow-xl"
              alt="Cart"
              width="32"
              height="32"
              />
          </Link>
            </div>
            </>
        )} />          
      </div>
      )
}

export default Nav