import React from "react"
import Image from './image';
import { Link } from "gatsby"

const CatalogProduct = ( props ) => {
  const styles = {
    display: "block",
    border: "2px solid #b2e1f5",
    background: "#fff",
    borderRadius: "10px",
    position: "relative",
    zIndex: "3",
    padding: "3px 40px 4px 16px",
    fontWeight: "bold",
    fontSize: "16px",
    marginTop: '-15px',
    textDecoration: 'none',
    color: '#005380'
  }

  console.log(props);

  return (
  <div className="catalog_product">
      <Image 
        style={{overflow: 'hidden', borderTopRightRadius: '8px', borderTopLeftRadius: '8px'}}
        src={props.src}
        alt={props.alt}
      />
      {props.link ? <Link className="product_name" style={styles} to={props.link}>{props.name}</Link> : <span className="product_name" style={styles}>{props.name}</span>}
  </div>
      )
}

export default CatalogProduct