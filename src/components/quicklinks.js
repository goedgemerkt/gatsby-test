import React from "react"
import Image from './image';
import '../css/quicklinks.css';

const Quicklinks = () => {

  return (
    <section className="quicklinks" style={{marginTop: '30px'}}>
    <div className="pink">
      <h2>Naamstickers</h2>
      <div className="image">
                <Image
                src="Mainimage_1_NL.jpg"
                className="mx-auto shadow-xl"
                alt="naamstickers"
                />
      </div>
        <ul>
          <li>Mini Naamstickers</li>
          <li>Kleine Naamstickers</li>
          <li>Design Naamstickers</li>
        </ul>
      </div>

      <div className="blue">
      <h2>Kledinglabels</h2>
      <div className="image">
                <Image
                src="MainImage_3_nl.jpg"
                className="mx-auto shadow-xl"
                alt="kledingstickers"
                />
      </div>
        <ul>
          <li>Kledingstickers</li>
          <li>Instrijklabels</li>
          <li>Kledingstempel</li>
        </ul>
      </div>

      <div className="green">
      <h2>Combivoordeel</h2>
      <div className="image">
                <Image
                src="Mainimage_2_NL.jpg"
                className="mx-auto shadow-xl"
                alt="Combivoordeel"
                />
      </div>
        <ul>
          <li>Babypakket</li>
          <li>Kinderdagverblijf</li>
          <li>Schoolpakket</li>
        </ul>
      </div>
    </section>
      )
}

export default Quicklinks