import React from "react"
import Image from './image';
import '../css/usps.css'

const Quicklinks = () => {

  return (
    <div class="sellingpoints-block" style={{
        borderRadius: '10px',
        border: '3px solid rgba(0,157,224,.2)',
        background: '#fff',
        margin: '0 0 30px',
        padding: '22px 45px 17px'
    }}>
        <ul>
        <li><Image
                src="toxic_ink.svg"
                className="mx-auto shadow-xl"
                alt="toxic ink"
                height="64"
                width="64"
                />Géén giftige inkten</li>
        <li><Image
                src="waterproof.svg"
                className="mx-auto shadow-xl"
                alt="waterproof"
                height="64"
                width="64"
                />Watervast</li>
        <li><Image
                src="dishwasher.svg"
                className="mx-auto shadow-xl"
                alt="dishwasher"
                height="64"
                width="64"
                />Vaatwasser veilig</li>
        <li><Image
                src="heat_safe.svg"
                className="mx-auto shadow-xl"
                alt="heat safe"
                height="64"
                width="64"
                />Magnetron veilig</li>
        <li><Image
                src="Delivery.svg"
                className="mx-auto shadow-xl"
                alt="quick delivery"
                height="64"
                width="64"
                />Snelle verzending</li>
        <li><Image
                src="Helpdesk.svg"
                className="mx-auto shadow-xl"
                alt="quick service"
                height="64"
                width="64"
                />9-21 uur</li>
        </ul>
    </div>
  )
}

export default Quicklinks