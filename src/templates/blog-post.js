const path = require(`path`)

exports.createPages = async ({ graphql, actions }) => {
    console.log('testttt')
  const { createPage } = actions
  const result = await graphql(`
    query MyQuery {
        goedgemerkt {
        categoryList {
            children {
            id
            name
            url_path
            }
        }
        }
    }
  `)

  const template = path.resolve('./src/templates/category.js')

  result.goedgemerkt.categoryList[0].children.forEach(category => {
      console.log(category)
      const catPath = category.url_path
      createPage({
          catPath,
          component: template,
          context: category,
      })
  })
}