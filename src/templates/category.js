import React from "react"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import CatalogProduct from '../components/catalogProduct'
import '../css/naamstickers.css'
import { graphql } from "gatsby"

const Category = (props, { data }) => {
console.log(props);

return (
  <Layout style={{ maxWidth: '1140px', margin: '0 auto' }}>
    <SEO title="Naamstickers" />
    <div className="grid">
    <ul>
        <li>Naamstickers</li>
        <li>Kleding merken</li>
        <li>Combivoordeel</li>
        <li>Veiligheid</li>
        <li>Voor volwassenen</li>
        <li>Diversen</li>
        <li>Cadeaubonnen</li>
    </ul>
    <div className="cat_prod">
        <div className="cat_img_wrapper">
            <Image src="Categorie_banner_naamstickers2_1.jpg"
            className="cat_img"
            alt="Naamstickers" />
            <div className="cat_info">
                <h1>Naamstickers</h1>
                <p>Met de naamstickers van Goedgemerkt zorg je ervoor dat je alle spullen van jouw kindje(s) <br />goed kunt merken, daardoor zullen ze niet<br />meer verwisseld worden of kwijtraken.</p>
            </div>
        </div>
        <div className="cat_products">
            {props.pageContext.products.map((product, i) => (
                <CatalogProduct 
                    src={`cat_1.jpg`}
                    alt={product.name}
                    name={product.name}
                    link="/mini-naamstickers"
                />
            ))}
        </div>
    </div>
    </div>
  </Layout>
)}

export default Category

