const path = require('path')
const { createRemoteFileNode } = require('gatsby-source-filesystem')

exports.createPages = async ({ graphql, actions, getCache, createNodeId, cache, reporter }) => {
    const { createPage, createNode } = actions
    
	const result = await graphql(`
    query MyQuery {
        goedgemerkt {
            categoryList {
                children {
                id
                name
                url_path
                products {
                    items {
                        id
                        name
                        url_path
                        }
                    }
                }
            }
        }
    } 
 `)
    const template = path.resolve('./src/templates/category.js')
    
	result.data.goedgemerkt.categoryList[0].children.forEach(async (category) => {
		const products = []
		category.products.items.forEach(item => {
			products.push({
				name: item.name,
				url: item.url_path,
			})
		})
		if (products.length > 0) {
			createPage({
				path: `/${category.url_path}`,
				component: template,
				context: {
					products: products,
					id: category.id.toString()
				}
			})
		}
	})
}

// exports.createSchemaCustomization = ({ actions }) => {
//     const { createTypes } = actions


//     createTypes(`
//     type SitePage implements Node {
//         frontmatter: Frontmatter
//         featuredImg: File @link(from: "featuredImg___NODE")
//     }

//     type Frontmatter {
//         title: String!
//         featuredImgUrl: String
//         featuredImgAlt: String
//       }
//   `)
//   }

//   exports.onCreateNode = async ({ node, actions: { createNode }, store, cache, createNodeId }) => {
    
//     // For all MarkdownRemark nodes that have a featured image url, call createRemoteFileNode
//     if (node.internal.type === "SitePage" && node.context) {
//         if (node.context.products){

//             console.log(true);

//             let fileNode = await createRemoteFileNode({
//                 url: "https://static.goedgemerkt.nl/pub/media/catalog/product/cache/a69088c64a6f850dbd3a331531bc43d9/m/i/minixs_nl_mainimage2_1.jpg", // string that points to the URL of the image
//                 parentNodeId: node.id, // id of the parent node of the fileNode you are going to create
//                 createNode, // helper function in gatsby-node to generate the node
//                 createNodeId, // helper function in gatsby-node to generate the node id
//                 cache, // Gatsby's cache
//                 store, // Gatsby's Redux store
//               })

//               console.log(fileNode);
//               // if the file was created, attach the new node to the parent node
//               if (fileNode) {
//                 node.featuredImg___NODE = fileNode.id
//               }
//         }
//     }
//   }